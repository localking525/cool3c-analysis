<?php
namespace Cool3c\Analysis\Matomo;
/**
 * Created by PhpStorm.
 * User: localking
 * Date: 2019/1/9
 * Time: 2:07 PM
 */
use Carbon\Carbon;
use Cool3c\Analysis\Contracts\AnalysisTool;
use Cool3c\Analysis\Page;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

/**
 * Class Matomo
 * @package Cool3c\Analysis
 * @property array $site_info
 * @property string $api_path
 * @property string $token
 * @property Client $client
 * @property $result
 * @property Page $page
 * @property string $default_site
 * @property array $query
 * @property int $cache_mins
 */
class Matomo implements AnalysisTool
{

    /**
     * @var array
     */

    public $page;

    protected $site_info = [];
    protected $api_path;
    protected $token;
    protected $client;
    protected $result;
    protected $query;

    private $default_site;
    private $date_int;
    private $cache_mins;

    /**
     * Matomo constructor.
     * @param Page $page
     */
    public function __construct(Page $page=null)
    {
        $this->cache_mins = config('cool3c_analysis.matomo.cache_minutes', 10);
        $this->token = config('cool3c_analysis.matomo.token');
        $this->api_path = config('cool3c_analysis.matomo.host');
        $this->default_site = config('cool3c_analysis.matomo.default', 'default');
        $this->setPage($page)
            ->setSite()
            ->setDate()
            ->setClient();
    }

    /**
     * @param string $siteName
     * @return $this
     */
    public function setSite($siteName = null)
    {
        $sites = config('cool3c_analysis.matomo.sites', []);
        $siteName = $siteName ?? $this->default_site;
        if(!empty($sites[$siteName])){
            $this->site_info = $sites[$siteName];
        }
        return $this;
    }

    /**
     * @param Carbon|null $date
     * @return $this
     */
    public function setDate(Carbon $date=null)
    {
        $this->date_int = empty($date) ? Carbon::now()->format('Ymd') : $date->format('Ymd');
        return $this;
    }

    /**
     * @return $this
     */
    public function setClient()
    {
        $this->client = new Client([
            'base_uri' => $this->api_path,
            // You can set any number of default request options.
            'timeout' => 10.0,
        ]);
        return $this;
    }

    /**
     * @param string|null $key
     * @return string
     * @throws Exception
     */
    protected function getCacheKey(string $key = null)
    {
        $this->validatePageInfo();
        $format = $format ?? 'piwik:%s:%d';
        $key = strtolower($key);
        $page_created_at = '';
        if(!empty($this->page->created_at)){
            $page_created_at = $this->page->created_at->toDateString();
        }
        switch ($key) {
            case 'transition':
                $format = 'piwik:transition:%s:%d:%s';
                break;
            default:
                $format = 'piwik:getPageUrl:%s:%d:%s';
        }
        return vsprintf($format, [
            md5($this->page->url),
            $this->date_int,
            $page_created_at
        ]);
    }

    /**
     * @param null $date
     * @return $this
     * @throws Exception
     */
    public function devicesDetection($date = null)
    {
        $this->validatePageInfo();
        $data = Cache::rememberForever(
            'piwik:devicesDetection',
            function () use ($date) {
                $date = $date ?? Carbon::now()->format('Y-m-d');
                $query = [
                    'module' => 'API',
                    'method' => 'DevicesDetection.getType',
                    'format' => 'JSON',
                    'idSite' => $this->site_info['site_id'],
                    'period' => 'day',
                    'date' => $date,
                    'token_auth' => $this->token,
                ];
                $response = $this->client->request('GET', 'index.php', [
                    'query' => $query
                ]);

                $data = $response->getBody();
                return @json_decode($data, true);
            }
        );


        $this->result = $data;

        return $this;
    }

    /**
     * @return $this|AnalysisTool
     * @throws Exception
     */
    public function doAnalytics()
    {

        $this->validatePageInfo();
        $query = $this->doAnalysisQuery();
        $this->result = Cache::remember($this->getCacheKey(), $this->cache_mins,
            function () use($query){
                $response = $this->client->request('GET', 'index.php', [
                    'query' => $query
                ]);

                $data = $response->getBody();
                return @json_decode($data, true);
            }
        );
        return $this;

    }

    /**
     * @return $this
     * @throws Exception
     */
    public function getTransitionsForPageUrl()
    {
        $this->validatePageInfo();
        $data = Cache::remember(
            $this->getCacheKey('transition'),
            15,
            function () {
                $date = Carbon::createFromFormat('Ymd', $this->date_int);
                $query = [
                    'module' => 'API',
                    'method' => 'Transitions.getTransitionsForAction',
                    'format' => 'JSON',
                    'idSite' => $this->site_info['site_id'],
                    'period' => 'day',
                    'date' => $date->toDateString(),
                    'token_auth' => $this->token,
                    'actionName' => $this->page->url,
                    'actionType' => 'url',
                    'parts' => 'all'
                ];

                $response = $this->client->request('GET', 'index.php', [
                    'query' => $query
                ]);

                $data = $response->getBody();
                return @json_decode($data, true);
            }
        );


        $this->result = $data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPv()
    {
        if(!empty($this->result)){
            return collect($this->result)
                ->collapse()
                ->sum('entry_nb_actions');
        }
        return "no result or error";
    }

    /**
     * @return mixed
     */
    public function getUu()
    {
        if(!empty($this->result)){
            return collect($this->result)
                ->collapse()
                ->sum('entry_nb_uniq_visitors');
        }
        return "no result or error";
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @throws Exception
     */
    public function validatePageInfo()
    {
        if (empty($this->page->url)) throw new Exception("Page url is empty");
    }

    /**
     * @param Page $page
     * @return Matomo
     */
    public function setPage(Page $page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return array|mixed
     */
    private function doAnalysisQuery()
    {
        $date = Carbon::createFromFormat('Ymd', $this->date_int);
        $path = parse_url($this->page->url, PHP_URL_PATH);
        $query = parse_url($this->page->url, PHP_URL_QUERY);
        if ($query) {
            $path = vsprintf("%s?%s", [
                $path,
                $query
            ]);
        }
        $date_str = $date->toDateString();
        if (!empty($this->page->created_at)) {
            if ($this->page->created_at->greaterThan($date)) {
                $date_str = vsprintf('%s,%s', [$date_str, $this->page->created_at->toDateString()]);
            } else {
                $date_str = vsprintf('%s,%s', [$this->page->created_at->toDateString(), $date_str]);
            }
        }


        $this->query = [
            'module' => 'API',
            'method' => 'Actions.getPageUrl',
            'format' => 'JSON',
            'idSite' => $this->site_info['site_id'],
            'period' => 'day',
            'date' => $date_str,
            'token_auth' => $this->token,
            'pageUrl' => $path
        ];
        return $this->query;
    }

    /**
     * @return array
     */
    protected function getQuery()
    {
        return $this->query;
    }

}