<?php
/**
 * Created by PhpStorm.
 * User: localking
 * Date: 2019/1/8
 * Time: 2:24 PM
 */
return [
    'matomo'=> [
        //From setting GuzzleHttp\Client base_uri
        'host' => env('MATOMO_HOST', 'https://abc.com/'),

        //Use Matomo API need on api token, to access data
        'token' => env('MATOMO_TOKEN'),

        //For Cache query result, you can use php artisan cache:clear to clean cache
        'cache_minutes'=> env('MATOMO_CACHE_MINS', 10),

        //Init Matomo Object will use this default site to run Analytics
        'default'=> env('MATOMO_DEFAULT_SITE', 'default'),
        'sites'=> [
            'default'=> [
                'site_id'=> 1,
                'url'=> 'https://www.example.com'
            ]
        ]
    ]


];