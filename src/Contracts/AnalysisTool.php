<?php
namespace Cool3c\Analysis\Contracts;
use Cool3c\Analysis\Page;

/**
 * Created by PhpStorm.
 * User: localking
 * Date: 2019/1/15
 * Time: 11:39 PM
 */
interface AnalysisTool
{
    /**
     * @return AnalysisTool
     */
    public function doAnalytics();

    /**
     * @return mixed
     */
    public function getPv();

    /**
     * @return mixed
     */
    public function getUu();

    /**
     * @return mixed
     */
    public function getResult();

    /**
     * @param Page $page
     * @return AnalysisTool
     */
    public function setPage(Page $page);
}