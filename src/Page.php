<?php
/**
 * Created by PhpStorm.
 * User: localking
 * Date: 2019/1/16
 * Time: 2:15 AM
 */
namespace Cool3c\Analysis;

use Carbon\Carbon;

/**
 * Class Page
 * @package cool3c\analysis
 * @property string $url
 * @property Carbon $created_at
 */
class Page
{
    public $url;
    public $created_at;

    public function __construct($url = null)
    {
        $this->url = $url;
    }

    /**
     * @param string $url
     * @return Page
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param Carbon $created_at
     * @return Page
     */
    public function setCreatedAt(Carbon $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }
}