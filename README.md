
## Installation

To get started with Cool3c Analysis:

    composer require cool3c/analysis
    
    
    
In Laravel 5.5 the service provider will automatically get registered. In older versions of the framework just add the service provider in `config/app.php` file:

```php
'providers' => [
    // ...
    Cool3c\Analysis\AnalysisServiceProvider::class,
];
```

You can publish the config:

```bash
php artisan vendor:publish --provider="Cool3c\Analysis\AnalysisServiceProvider"
```

## Baseic Usage


### First 

```
use Cool3c\Analysis\Matomo\Matomo;
use Cool3c\Analysis\Page;
```


### Second

```

$url = 'https://example.com/article/123456' //page Url 
$page = new Page();
$page->setUrl($url)
    ->setCreatedAt(Carbon::create(2019, 1, 17));

$matomo = new Matomo($page);
$result = $matomo->doAnalytics() //Run Matomo API Actions.getPageUrl
    ->getResult();  // Get Result without parsing
    
    
$pv = $matomo->getPv();  //Sum entry_nb_actions in results
$uu = $matomo->getUu();  //Sum entry_nb_uniq_visitors in results

```


### Page methods

Init 
```
$page = new Page($url);

```
Set Page Url
```
$page->setUrl($url);
```

Set Page Create Date, need to use Carbon instance
```
$page->setCreatedAt(Carbon\Carbon::now());
```

### Matomo methods

Set Page Object to Matomo
```
$matomo->setPage($page);
//return Cool3c\Analysis\Matomo\Matomo;
```

Set run Analytics site name which you set in cool3c_analysis.php sites
```
$matomo->setSite('default') 
//return Cool3c\Analysis\Matomo\Matomo;
```

Set Date to Matomo for running Analytics the End of the Date

For example: Page create at 2019-1-1, then matomo defualt date is Carbon::now()
, basically it will run analytics in 2019-1-1 ~ now.

```
$matomo->setDate(Carbon\Carbon::now());
//return Cool3c\Analysis\Matomo\Matomo;
```


Run Analytics, first will check page url, if empty will throw Exception
```
$matomo->doAnalytics()
```

Get Results after running Analytics
```
$matomo->getResult();
```

Get page views of the Page,  sum [entry_nb_actions](https://developer.matomo.org/api-reference/reporting-api#Actions) in results
```
$matomo->getPv();

```

Get Unique users of the Page, sum [entry_nb_uniq_visitors](https://developer.matomo.org/api-reference/reporting-api#Actions) in results
```
$matomo->getUu();
```